package br.ucsal.bes20201.testequalidade.locadora;

import br.ucsal.bes20201.testequalidade.locadora.business.*;
import br.ucsal.bes20201.testequalidade.locadora.persistence.*;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import br.ucsal.bes20201.testequalidade.locadora.builder.*;

public class LocacaoBOIntegradoTest {

	/**
	 * Testar o calculo do valor de loca��o por 3 dias para 2 veiculos com 1 ano de
	 * fabrica�ao e 3 veiculos com 8 anos de fabrica��o.
	 */

	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() {
		List<Veiculo> veiculos = new ArrayList <Veiculo>();
		
		Veiculo veiculo1 = VeiculoBuilder.veiculoDefault().comAno(2012).build();
		Veiculo veiculo2 = VeiculoBuilder.veiculoDefault().comAno(2012).build();
		Veiculo veiculo3 = VeiculoBuilder.veiculoDefault().comAno(2012).build();
		Veiculo veiculo4 = VeiculoBuilder.veiculoDefault().comAno(2019).build();
		Veiculo veiculo5 = VeiculoBuilder.veiculoDefault().comAno(2019).build();
		
		VeiculoDAO.insert(veiculo1);
		VeiculoDAO.insert(veiculo2);
		VeiculoDAO.insert(veiculo3);
		VeiculoDAO.insert(veiculo4);
		VeiculoDAO.insert(veiculo5);
		
		veiculos.add(veiculo1);
		veiculos.add(veiculo2);
		veiculos.add(veiculo3);
		veiculos.add(veiculo4);
		veiculos.add(veiculo5);
		
		assertEquals(2115, LocacaoBO.calcularValorTotalLocacao(veiculos, 3));		
	
	}

}
