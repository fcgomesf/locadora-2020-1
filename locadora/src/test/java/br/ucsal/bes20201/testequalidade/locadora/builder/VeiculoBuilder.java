package br.ucsal.bes20201.testequalidade.locadora.builder;

import br.ucsal.bes20201.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {

	public final static String DEFAULT_PLACA = "abc1234";
	public final static Integer DEFAULT_ANO = 1990;
	public final static Modelo DEFAULT_MODELO = new Modelo("Carro r�pido");
	public final static Double DEFAULT_VALOR_DIARIA = 150.00;
	public final static SituacaoVeiculoEnum DEFAULT_SITUACAO = SituacaoVeiculoEnum.DISPONIVEL;

	private String placa;
	private Integer ano;
	private Modelo modelo;
	private Double valorDiaria;
	private SituacaoVeiculoEnum situacao;

	private VeiculoBuilder() {
	};

	public static VeiculoBuilder umVeiculoBuilder() {
		return new VeiculoBuilder();
	}

	public VeiculoBuilder comPlaca(String placa) {
		this.placa = placa;
		return this;
	}

	public VeiculoBuilder comAno(Integer ano) {
		this.ano = ano;
		return this;
	}

	public VeiculoBuilder comModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}

	public VeiculoBuilder comValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;
	}

	public VeiculoBuilder comSituacao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao;
		return this;
	}

	public VeiculoBuilder disponivel() {
		this.situacao = SituacaoVeiculoEnum.DISPONIVEL;
		return this;
	}

	public VeiculoBuilder locado() {
		this.situacao = SituacaoVeiculoEnum.LOCADO;
		return this;
	}

	public VeiculoBuilder manutencao() {
		this.situacao = SituacaoVeiculoEnum.MANUTENCAO;
		return this;
	}

	public VeiculoBuilder but() {
		return umVeiculoBuilder()
				.comAno(ano)
				.comModelo(modelo)
				.comPlaca(placa)
				.comValorDiaria(valorDiaria)
				.comSituacao(situacao);
	}

	public Veiculo build() {
		Veiculo veiculo = new Veiculo();
		veiculo.setAno(this.ano);
		veiculo.setModelo(this.modelo);
		veiculo.setPlaca(this.placa);
		veiculo.setSituacao(this.situacao);
		veiculo.setValorDiaria(this.valorDiaria);
		return veiculo;
	}


	public static VeiculoBuilder veiculoDefault() {
		return VeiculoBuilder.umVeiculoBuilder()
				.comAno(DEFAULT_ANO)
				.comModelo(DEFAULT_MODELO)
				.comPlaca(DEFAULT_PLACA)
				.comSituacao(DEFAULT_SITUACAO)
				.comValorDiaria(DEFAULT_VALOR_DIARIA);
	}

}
