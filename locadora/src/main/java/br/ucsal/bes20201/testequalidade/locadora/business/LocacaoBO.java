package br.ucsal.bes20201.testequalidade.locadora.business;

import java.time.LocalDate;
import java.util.List;

import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;

public class LocacaoBO {

	/**
	 * Calcula o valor total da loca��o dos veiculos para uma quantidade de dias.
	 * Veiculos com mais de 5 anos de fabrica��o tem desconto de 10%.
	 * 
	 * @param veiculos              veiculos que ser�oo locados 
	 * @param quantidadeDiasLocacao quantidade de dias de loca��o
	 * @return
	 */
	public static Double calcularValorTotalLocacao(List<Veiculo> veiculos, Integer quantidadeDiasLocacao) {
		Double total = 0d;
		Double valorLocacaoVeiculo;
		Integer anoAtual = LocalDate.now().getYear();

		for (Veiculo veiculo : veiculos) {
			valorLocacaoVeiculo = veiculo.getValorDiaria() * quantidadeDiasLocacao;
			if (veiculo.getAno() < anoAtual - 5) {
				valorLocacaoVeiculo *= .9;
			}
			total += valorLocacaoVeiculo;
		}

		return total;
	}

}
